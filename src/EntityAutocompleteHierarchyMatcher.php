<?php

namespace Drupal\entity_autocomplete_hierarchy;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityAutocompleteMatcher;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * A service that decorates the entity.autocomplete_matcher service.
 *
 * It shouldn't need to extend the EntityAutocompleteMatcher, however, that
 * service has no interface no there is no other way.
 *
 * @package Drupal\entity_autocomplete_hierarchy
 */
class EntityAutocompleteHierarchyMatcher extends EntityAutocompleteMatcher {

  /**
   * The original service.
   *
   * @var \Drupal\Core\Entity\EntityAutocompleteMatcher
   */
  protected $entityAutocompleteMatcher;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity repository interface.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * A mapping of entity types to parent fields.
   *
   * @var array
   */
  protected $parentFields;

  /**
   * The ancestor separator.
   *
   * @var String
   */
  protected $separator;

  /**
   * Constructs a EntityAutocompleteMatcher object.
   *
   * @param array $parentFields
   *   An array of supported entity types with the name of the 'parent' field.
   * @param String $separator
   * @param \Drupal\Core\Entity\EntityAutocompleteMatcher $entityAutocompleteMatcher
   *   The original service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   */
  public function __construct(array $parentFields, String $separator, EntityAutocompleteMatcher $entityAutocompleteMatcher, EntityTypeManagerInterface $entityTypeManager, EntityRepositoryInterface $entityRepository) {
    $this->parentFields = $parentFields;
    $this->separator = $separator;
    $this->entityAutocompleteMatcher = $entityAutocompleteMatcher;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityRepository = $entityRepository;
  }

  /**
   * Gets matched labels based on a given search string.
   *
   * @param string $target_type
   *   The ID of the target entity type.
   * @param string $selection_handler
   *   The plugin ID of the entity reference selection handler.
   * @param array $selection_settings
   *   An array of settings that will be passed to the selection handler.
   * @param string $string
   *   (optional) The label of the entity to query by.
   *
   * @return array
   *   An array of matched entity labels, in the format required by the AJAX
   *   autocomplete API (e.g. array('value' => $value, 'label' => $label)).
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @see \Drupal\system\Controller\EntityAutocompleteController
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {
    $matches = $this->entityAutocompleteMatcher->getMatches($target_type, $selection_handler, $selection_settings, $string);

    if ($this->getParentField($target_type)) {
      foreach ($matches as &$match) {
        $id = $this->getId($match['value']);
        $entity = $this->entityTypeManager->getStorage($target_type)->load($id);
        $trail = $this->getAncestors($entity);
        if (!empty($trail)) {
          $match['label'] = implode($this->separator, $trail);
        }
      }
    }

    return $matches;
  }

  /**
   * Return the 'parent' field for a given entity type.
   *
   * @param String $entityTypeId
   *   The entity type id.
   *
   * @return String|NULL
   */
  protected function getParentField(String $entityTypeId) {
    if (isset($this->parentFields[$entityTypeId])) {
      return $this->parentFields[$entityTypeId];
    }
  }

  /**
   * Extracts the id from the item key.
   *
   * @param String $key
   *   The label of the entity followed by its id in parenthesis.
   *
   * @return String|null
   */
  protected function getId(String $key) {
    $matches = [];
    if (preg_match('/\((?<id>[^)]+)\)["]?$/', $key, $matches)) {
      return $matches['id'];
    }
    return NULL;
  }

  protected function getAncestors(FieldableEntityInterface $entity, $ids = []) {
    $parentField = $this->getParentField($entity->getEntityTypeId());
    if (in_array($entity->id(), $ids) || !$entity->hasField($parentField)) {
      // Circular reference or malformed entity detected. Bail out.
      return [];
    }

    $ids[] = $entity->id();

    $parent = $entity->{$parentField}->entity;
    $ancestors = $parent ? $this->getAncestors($parent, $ids) : [];

    $ancestors[] = Html::escape($this->entityRepository
      ->getTranslationFromContext($entity)
      ->label());

    return $ancestors;
  }

}
