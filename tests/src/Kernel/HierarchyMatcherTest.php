<?php

namespace Drupal\Tests\entity_autocomplete_hierarchy\Kernel;

use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\token\Kernel\KernelTestBase;

/**
 * Kernel tests for taxonomy term matching.
 *
 * @group taxonomy
 */
class HierarchyMatcherTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'filter',
    'entity_autocomplete_hierarchy',
    'taxonomy',
    'text',
    'user'
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installConfig(['filter']);
    $this->installEntitySchema('taxonomy_term');
  }

  /**
   * Tests the taxonomy matching.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testTaxonomyMatching() {
    $vid = 'test_vocabulary';
    Vocabulary::create([
      'name' => 'Test vocabulary',
      'vid' => $vid,
    ])->save();
    $parent = Term::create([
      'name' => 'Parent term',
      'vid' => $vid,
    ]);
    $parent->save();
    $child = Term::create([
      'name' => 'Child term',
      'vid' => $vid,
      'parent' => $parent->id(),
    ]);
    $child->save();

    $matches = $this->container->get('entity.autocomplete_matcher')
      ->getMatches('taxonomy_term', 'default', [], 'term');
    $this->assertEquals('Parent term > Child term', $matches[0]['label']);
    $this->assertEquals('Parent term', $matches[1]['label']);
  }

}
